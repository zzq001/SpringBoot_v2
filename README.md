该系统整合：
1. Swagger2.0
2. thymeleaf
3. mybatis2-generator.xml
4. 多环境
5. 一个整理好的后台模版
6. 自定义拦截
7. 自定义定时器
8. pagehelper分页插件
9. 权限security

![界面样式](https://images.gitee.com/uploads/images/2018/0822/003000_175616d0_123301.png "{{H[HML}(`V33A47(U)ZTF5.png")
![HTML代码页面](https://images.gitee.com/uploads/images/2018/0822/004608_c55d62a4_123301.jpeg "未命名拼图 (1).jpg")